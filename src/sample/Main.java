package sample;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Main extends Application {

        public final Canvas canvas = new Canvas(1000,1000);
        public final GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        public final double xCentre = canvas.getWidth() / 2;
        public final double yCentre =canvas.getHeight() / 2;
        public double radiusInside = 300;
        public static final int N_POINTS = 3;

        @Override
        public void start(Stage primaryStage) throws Exception{
            Group root = new Group();
            recurTriangleSerpinskyDraw(radiusInside,xCentre,yCentre);
            root.getChildren().add(canvas);
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        }



        public void triangleDraw(double radius, double xCentre, double yCentre){
            double xPoints[] = {xCentre-2*radius, xCentre + 2*radius, xCentre};
            double yPoints[] = {yCentre + radius, yCentre + radius,yCentre - 2*radius};
            graphicsContext.setFill(Color.BLACK);
            graphicsContext.fillPolygon(xPoints, yPoints, N_POINTS);

        }

        public void triangleInvertDraw(double radius, double xCentre, double yCentre){
            double aX = xCentre-2*radius;
            double bX = xCentre+2*radius;
            double cX = xCentre;
            double aY = yCentre + radius;
            double bY = yCentre + radius;
            double cY = yCentre - 2*radius;
            double xPoints[] = {(aX+bX)/2,(bX+cX)/2, (cX+aX)/2};
            double yPoints[] = {(aY+bY)/2,(bY+cY)/2, (cY+aY)/2};
            graphicsContext.setFill(Color.WHITE);
            graphicsContext.fillPolygon(xPoints, yPoints, N_POINTS);

       }

        public void recurTriangleSerpinskyDraw(double radius,double xCentre, double yCentre) {
            if (radius > 20) {
                triangleDraw(radius, xCentre, yCentre);
                triangleInvertDraw(radius, xCentre, yCentre);
                if ((radius /= 2) > 20) {
                    recurTriangleSerpinskyDraw(radius, xCentre, yCentre-2*radius);

                    recurTriangleSerpinskyDraw(radius, xCentre + 2.3*(Math.sqrt(3)/2)*((3/2)*radius), yCentre + radius );
                    //                h = (3/2)*radius;

                    recurTriangleSerpinskyDraw(radius, xCentre - 2.3*(Math.sqrt(3)/2)*((3/2)*radius), yCentre + radius);

                }
            }
        }

        public static void main(String[] args) {
            launch(args);
        }
    }